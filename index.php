<?php
use oopblog\classes\User;
use oopblog\classes\Session;
use oopblog\classes\Cookie;
use oopblog\classes\Config;
use oopblog\classes\DB;


require_once('core/init.php');

$user = new User(new DB(new Config), new Session, new Cookie, new Config);

if( $user->isLoggedIn() )
{
	?>
	Hi, <?php echo $user->data()->username; ?>
	<ul>
		<li><a href="changepassword.php">Change Password</a></li>
		<li><a href="update.php">Update</a></li>
		<li><a href="profile.php">Profile</a></li>
		<li><a href="logout.php">Logout</a></li>
	</ul>


<?php
} else {

	echo "You are not logged in. Please <a href='register.php'>register</a> or <a href='login.php'>login</a>";
}
