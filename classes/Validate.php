<?php
namespace oopblog\classes;

class Validate
{

	private $_db,
			$_errors = [],
			$_passed = false;



	public function check($source, $items = array(), DB $db)
	{


		foreach($items as $item => $rules)
		{


			foreach($rules as $rule => $rule_value)
			{


				$value = escape($source[$item]);



				if($rule === 'required' && empty($value))
				{


					$this->addError("{$item} is empty");

				}


				switch($rule)
				{

					case'min':

						if(strlen($value) < $rule_value)
						{

							$this->addError("{$item} must be minimum {$rule_value} characters");
						}

					break;

					case'max':

						if(strlen($value) > $rule_value)
						{

							$this->addError("{$item} must be maximum {$rule_value} characters");
						}


					break;

					case'matches':

						if($source[$rule_value] != $value)
						{

							$this->addError("{$item} must mutch {$rule_value}");

						}


					break;

					case'unique':

					$this->_db = $db;
				
					$check = $this->_db->get($rule_value, array($item, '=', $value));
					if($this->_db->count())
					{

						$this->addError("{$item} must be unique");

					}

					break;
				}	

			}

		}

		if(empty($this->_errors))
		{

			$this->_passed = true;

		}


		return $this;


	}

	public function addError( $error )
	{

		$this->_errors[] = $error;

	}

	public function errors()
	{

		return $this->_errors;
	}

	public function passed()
	{

		return $this->_passed;

	}

}