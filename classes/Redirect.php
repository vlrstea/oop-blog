<?php
namespace oopblog\classes;

class Redirect
{


	public function to( $location )
	{

		if( $location )
		{

			if( is_numeric( $location ) )
			{

				switch($location)
				{

					case'404':

						header('HTTP/1.0 404 File Not Found');
						exit();

					break;

				}

			}

			header('Location:' . $location);
			exit();
		}

		return false;
	}
}