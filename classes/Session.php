<?php
namespace oopblog\classes;

class Session
{

	public function exists($name)
	{

		return (isset($_SESSION[$name])) ? true : false;

	}

	public function get($name)
	{

		return $_SESSION[$name];

	}

	public function put($name, $value)
	{

		return $_SESSION[$name] = $value;


	}


	public function delete($name)
	{

		if($this->exists($name))
		{

			unset($_SESSION[$name]);

		}
	}

	public function flash( $name, $message = null )
	{

		if( $this->exists( $name ) )
		{

			$session = $this->get( $name );
			$this->delete( $name );

			return $session;
		}	

		$this->put( $name, $message );

	}

}