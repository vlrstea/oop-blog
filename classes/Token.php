<?php
namespace oopblog\classes;
use oopblog\classes\Session as Session;
use oopblog\classes\Cookie as Cookie;

class Token

{

	private $_session,
			$_config;


	public function __construct(Config $config, Session $session)
	{

		$this->_config = $config;
		$this->_session = $session;

	}

	public function generate()
	{

		return $this->_session->put($this->_config->get('session/session_name'), base64_encode(random_bytes(32)));

	}

	public function check($token)
	{

		$tokenName = $this->_config->get('session/session_name');
		

		if($this->_session->exists($tokenName) && $this->_session->get($tokenName) === $token)
		{

			$this->_session->delete($tokenName);
			
			return true;
		}

		return false;
	}
}