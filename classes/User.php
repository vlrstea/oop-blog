<?php
namespace oopblog\classes;

use oopblog\classes\Config as Config;
use oopblog\classes\Session as Session;
use oopblog\classes\Cookie as Cookie;


class User
{

	private $_db,
			$_session,
			$_sessionName,
			$_cookie,
			$_config,
			$_data,
			$_isLoggedIn = false;



	public function __construct( DB $db, Session $session, Cookie $cookie, Config $config, $user = null)
	{	


		$this->_db = $db;
		$this->_session = $session;
		$this->_cookie = $cookie;
		$this->_config = $config;

		//Names from the config
		$this->_sessionName = $this->_config->get( 'session/session_name' );
	
		//if $user is set, find the user based on the provided id
		//if user is not set, check if a session exists
		//if a session exists, return the user id from the session and set isloggedin to true
		// else logout

		
		if ( !$user )
		{

			if( $this->_session->exists( $this->_sessionName ) )
			{

				$user = $this->_session->get( $this->_sessionName );
				
				if( $this->find( $user ) )
				{

					$this->_isLoggedIn = true;

				} else {

					$this->logout();
				}
				

			} 

		} else {


			$this->find( $user );
		}


	}



	// Creating an user account
	public function create( $fields = array() )
	{	
		//inserting the fields in the DB via DB insert method
		if( !$this->_db->insert('users', $fields) )
		{

			throw new \Exception('There\'s been a problem creating your account. - create method - user class');
		}
	}


	//update fields
	public function update( $fields = array(), $id = null )
	{	
		//check if has been provided an ID, else get the current user id
		$id = ($id === NULL) ? $id = $this->_data->id : $id = $id;

		if( !$this->_db->update('users', $id, $fields) )
		{

			throw new \Exception('There\'s been a problem updating.');
		}
	}



	public function find( $username )
	{

		if( $username )
		{

			$user = ( is_numeric( $username )) ? 'id' : 'username';
			$check = $this->_db->get('users', array($user, '=', $username));

			if($check->count())
			{
				$this->_data = $check->first();

				return true;
			}


		}

		return false;

	}

	public function login($username = null, $password = null, $rememberme = false)
	{

		if( !$username && !$password && $this->exists())
		{

			//if the session doesn't exists one will be created based on the cookie(core/init.php)
			$this->_session->put( $this->_config->get('session/session_name'), $this->_data->id);

		} 	else {

			//check for an account with the username provided;
			$user = $this->find( $username );

			if($user)
			{
				//the user exists, check if the passwords === with the one from the DB

				if(password_verify($password, $this->_data->password))
				{
					
					
					//create a session with current account logged in
					$this->_session->put( $this->_config->get('session/session_name'), $this->_data->id);


					//if the checkbox remember me is on
					if($rememberme)
					{

						//verify if the user is registered in the user_sessions with a hash
					 	$checkHash = $this->_db->get('user_sessions', array('user_id', '=', $this->_data->id));

					 	if($checkHash->count())
					 	{	
					 		//get the hash to create the cookie
					 		$hash = $checkHash->first()->hash;

					 	} else {
					 		
					 		//create and insert the hash in the DB
					 		$hash = utf8_encode(random_bytes( 64 ));
					 		$this->_db->insert('user_sessions', array(

					 			'user_id' => $this->_data->id,
					 			'hash' => $hash

					 			));
					 	}

					 	//create a cookie with the user id and a hash
					 	
					 	$this->_cookie->put( $this->_config->get( 'remember/cookie_name' ), $hash, $this->_config->get( 'remember/cookie_expiry') );
						
					}

					

					return true;
				}

			}
		}

		return false;
	}

	//return data collected from DB based on the id / username provided in the "find" method
	public function data()
	{

		return $this->_data;
	}


	public function exists()
	{

		return ( !empty($this->_data) ) ? true : false;
	}

	public function logout()
	{

		if( $this->_isLoggedIn )
		{

			//delete the session associated with the current account
			$this->_session->delete( $this->_config->get('session/session_name') );

			//delete the cookie associated with the current account
			$this->_cookie->delete( $this->_config->get('remember/cookie_name') );
			//remove the hash from the database(user_sessions)
			$this->_db->delete( 'user_sessions', array('user_id', '=', $this->_data->id) );

		}

	}


	public function isLoggedIn()
	{

		return $this->_isLoggedIn;
	}

}
