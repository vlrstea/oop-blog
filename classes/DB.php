<?php
namespace oopblog\classes;
use oopblog\classes\Config as Config;

class DB
{

	private $_pdo,
			$_query,
			$_results,
			$_count = 0,
			$_error = false,
			$con;


	public function __construct(Config $config)
	{	

		$this->_con = $config;
		
		try
		{

			
			$this->_pdo = new \PDO('mysql:host='. $this->_con->get('mysql/host') .';dbname=' . $this->_con->get('mysql/database'), $this->_con->get('mysql/user'), $this->_con->get('mysql/password'));
			
		}
		 catch(PDOException $e)
		 {


		 	die( $e->getMessage() );

		 }

	}

	public function query( $sql, $values = array() )

	{
		
		$this->_error = false;


		if($this->_query = $this->_pdo->prepare( $sql ))
		{

			if( count($values) )
			{

				$num = 1;

				foreach( $values as $value )

				{

					$this->_query->bindValue($num, $value);

					$num++;
				}

			}


			if($this->_query->execute())
			{

				$this->_results = $this->_query->fetchAll(\PDO::FETCH_OBJ);
				$this->_count = $this->_query->rowCount();

			} else {

				echo 'error';
				$this->_error = true;
			}


		}

		return $this;

	}


	public function action($action, $table, $where = array())
	{	

		if(count($where === 3))
		{

			$operators = array('=', '<', '>', '<=', '>=');

			$field = $where[0];
			$operator = $where[1];
			$value = $where[2];

			if(in_array($operator, $operators))
			{

				$sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";


				if( !$this->query( $sql, array($value) )->error())
				{

					return $this;

				}
			}

		}

		return false;

	}

	public function get($table, $where = array())
	{

		return $this->action('SELECT *', $table, $where);

	}


	public function delete($table, $where = array())
	{

		return $this->action('DELETE', $table, $where);

	}

	public function insert( $table, $fields = array() )
	{
		
		$keys = array_keys( $fields );

		$values = null;
		$count = 1;

			foreach( $fields as $key => $value )
			{

				$values .= '?';

				if( $count < count($fields) )
				{

					$values .= ', ';
				}

				$count++;
			}


		
		$sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";
	
		if(!$this->query( $sql, $fields )->error())
		{

			return true;

		}

		return false;
	}

	public function update($table, $id, array $fields)
	{

		$set = '';
		$count = 1;

			foreach($fields as $key => $value)
			{

				$set .= $key . ' = ?';

				if($count < count($fields))
				{

					$set .= ', ';
				}

				$count++;
			}


		$sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";
		
		if(!$this->query($sql, $fields)->error())
		{

			return true;

		}

		return false;
	}

	//object array with all results
	public function results()
	{

		return $this->_results;


	}


	//get the first row of a query
	public function first()
	{

		return $this->_results[0];

	}

	//returns the number of rows from the query
	public function count()
	{

		return $this->_count;

	}


	public function error()
	{


		return $this->_error;

	}

}