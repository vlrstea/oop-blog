<?php
namespace oopblog\classes;

class Cookie
{

	public function exists( $name )
	{
		
		return (isset($_COOKIE[$name])) ? true : false;
	}

	public function get( $name )
	{

		return $_COOKIE[$name];

	}

	public function put( $name, $value, $expiry )
	{
		
		setcookie( $name, $value, time() + $expiry, '/');

	}

	public function delete( $name )
	{

		if($this->exists( $name ))
		{

			setcookie($name, '', time() - 1);
		}	

		return false;

	}
}