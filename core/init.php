<?php
namespace oopblog;
use oopblog\classes\DB;
use oopblog\classes\Config;
use oopblog\classes\Session;
use oopblog\classes\Cookie;
use oopblog\classes\User;

session_start();
error_reporting( E_ALL );
ini_set( "display_errors", 1 );


//Constants

define('ENVIRONMENT', 'local');

define('SITE_ROOT', 'C:\Apache24\htdocs');


/*//data
$GLOBALS['data'] = array(

		'remember' => array(

			'cookie_name' => 'hash',
			'cookie_expiry' => 86400

			),
		'session' => array(

			'session_name' => 'user',
			'token_name' => 'token'

			)

		);*/

//DATABASE connection data
if(ENVIRONMENT == 'production'){

	/* 
	
		Production datas

	$GLOBALS['data'] = array(); 


	*/

} else if(ENVIRONMENT == 'local'){

	$GLOBALS['data'] = array(

		'mysql' => array(

			'host' => '127.0.0.1',
			'user' => 'root',
			'password' => 'parola',
			'database' => 'oopblog',
			'charset' => 'utf-8'

			),
		'remember' => array(

			'cookie_name' => 'hash',
			'cookie_expiry' => 86400

			),
		'session' => array(

			'session_name' => 'user',
			'token_name' => 'token'

			)
		);
}


//autoloading classes and require files


spl_autoload_register(function($class){

	$filename = SITE_ROOT . '\\' . $class . '.php';

	if(! file_exists($filename)){
		
		return false;
	}

	require_once($filename);

});

require_once('functions/escape.php');


/*//Register services for Dependency Injection

$di = new DI;
$di->register('database', function() use ( $connection ){

	$obj = new DB();
	$obj->host = $connection['mysql']['host'];
	$obj->user = $connection['mysql']['user'];
	$obj->password = $connection['mysql']['password'];
	$obj->db = $connection['mysql']['database'];

	return $obj;
});
*/



//Rewriting the session based on the cookie to relogin the account
$config = new Config;
$db = new DB( $config );
$session = new Session;
$cookie = new Cookie;
$cookieName = $config->get( 'remember/cookie_name' );

//if the cookie exists and the session !=
if( $cookie->exists( $cookieName ) && !$session->exists( $config->get( 'session/session_name' ) ))
{

	//verify if the hash from user_session table(db) === with the one from the cookie
	$checkHash = $db->get('user_sessions', array('hash', '=', $cookie->get( $cookieName )));

	if( $checkHash->count() )
	{

		$user = new User($db, $session, $cookie, $config, $checkHash->first()->user_id);
		$user->login(  );
	}

}