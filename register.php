<?php
namespace oopblog;
use oopblog\classes\Token as Token;
use oopblog\classes\Config as Config;
use oopblog\classes\Session as Session;
use oopblog\classes\Input as Input;
use oopblog\classes\Validate as Validate;
use oopblog\classes\DB as DB;
use oopblog\classes\User as User;

require_once('core/init.php');

$input = new Input;
$token = new Token(new Config, new Session);

if($input->exists())
{

	if($token->check($input->get('token')))
	{

		$validate = new Validate;
		$validation = $validate->check($_POST, array(


				'username' => array(

					'required' => true,
					'min' => 3,
					'max' => 64,
					'unique' => 'users'

					),
				'password' => array(

					'required' => true,
					'min' => 2,

					),
				'password_again' => array(

					'required' => true,
					'matches' => 'password'

					),
				'name' => array(

					'required' => true,
					'min' => 2,
					'max' => 64

					)

			), new DB(new Config));


		if($validation->passed())
		{

			$user = new User(new DB(new Config), new Session, new Cookie, new Config);
			$password = password_hash($input->get('password'), PASSWORD_DEFAULT);

			$user->create( array(

				'username' => $input->get('username'),
				'password' => $password,
				'name' => $input->get('name'),
				'joined' => date('Y-m-d H:i:s'),
				'group' => '1'

				) );
			
		} else {

			foreach($validation->errors() as $error)
			{

				echo $error . '<br>';


			}
		}


	}

}

?>



<form action="" method="post">
	
	<div class="field">
		<label for="username">Username</label>
		<input type="text" name="username" id="username" value=""">
	</div>

	<div class="field">
		<label for="password">Password: </label>
		<input type="password" name="password" id="password">
	</div>

	<div class="field">
		<label for="password_again">Password again:</label>
		<input type="password" name="password_again" id="password_again">
	</div>

	<div class="field">
		<label for="name">Name: </label>
		<input type="text" name="name" id="name">
	</div>
	

	<input type="hidden" name="token" value="<?php echo $token->generate(); ?>">
	<input type="submit" value="Register">

</form>