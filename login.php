<?php

use oopblog\classes\DB as DB;
use oopblog\classes\User as User;
use oopblog\classes\Token as Token;
use oopblog\classes\Input as Input;
use oopblog\classes\Cookie as Cookie;
use oopblog\classes\Config as Config;
use oopblog\classes\Session as Session;
use oopblog\classes\Validate as Validate;
use oopblog\classes\Redirect as Redirect;


require_once('core/init.php');
$token = new Token(new Config, new Session);
$input = new Input();
$redirect = new Redirect;


//Check if an method exists
if($input->exists())
{
	//check for csrf token
	if($token->check($input->get('token'))){

		$validate = new Validate;

		//validate the fields thorugh the Validate class
		// issue - DB and Config are not necessary here
		$validation = $validate->check($_POST, array(

			'username' => array(

				'required' => true,
				'min' => 2,
				'max' => 64
				),
			'password' => array(

				'required' => true,
				'min' => 2
				)

			), new DB(new Config));

		//if validation passed, proceed to login the client;
		if($validation->passed())
		{

			$user = new User(new DB(new Config), new Session, new Cookie, new Config);
			$rememberme = ($input->get('rememberme') === 'on') ? true : false;
			$login = $user->login($input->get('username'), $input->get('password'), $rememberme);

			if($login)
			{

				$redirect->to('index.php');
			}


		} else {

			foreach($validation->errors as $error)
			{

				echo $error . '<br>';

			}

		}
	}

}
?>

<form action="" method="POST">
	
	
	<div class="field">
		
		<label for="username">Username</label>
		<input type="text" id="username" name="username" autocomplete="off" value="">

	</div>

	<div class="field">
		
		<label for="password">Password</label>
		<input type="password" id="password" name="password">

	</div>
	
	<label for="rememberme">
		<input type="checkbox" name="rememberme" id="rememberme">
		Remember me
	</label>

	<input type="hidden" name="token" value="<?php echo $token->generate();?>">
	<input type="submit" value="Login">
</form>